class ApplicationController < ActionController::Base
	include Pundit
  	#protect_from_forgery with: :exception
  	#before_action :authenticate_admin_usuario!
  	rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  
	def reserva
    	permitted = params.require(:basicbooking).permit(:arrival, :departure,:name,:cel,:enum_type)
    	permitted[:enum_type] = Basicbooking.enum_types.key(permitted[:enum_type].to_i)
    	basicbooking = Basicbooking.new(permitted)
    	basicbooking.save
    	head :ok
  	end

private

  def user_not_authorized
    flash[:alert] = "Você não tem autorização para acessar este conteudo."
    redirect_to("/entrar")
  end

  def pundit_user
    current_user
  end

end
