class InicioController < ApplicationController
  def index
    @sobre = About.last
    @sliders = Slider.all
    @carousels = Carousel.all
  end
end
