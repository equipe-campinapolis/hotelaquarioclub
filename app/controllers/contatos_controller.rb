class ContatosController < ApplicationController
  def index
  	@promocao = Promotion.where(visibility: true).last
  	@como_chegar = Contact.last
  	@items_como_chegar = @como_chegar.contactsections unless @como_chegar.nil?
  end
end
