class Service < ApplicationRecord
  has_many_attached :images

  validates :images, presence: true

	attr_accessor :remove_primary_image,:remove_images
	attribute :remove_images, :json
  after_save :delete_images, if: -> { remove_images.present? }
  
  def delete_images
    images.each_with_index { |_, index| images[index].purge if remove_images.include?(index.to_s) }
  end

   after_save do
    primary_image.purge if remove_primary_image == '1'
    Array(remove_images).each do |id|
      images.find_by_id(id).try(:purge)
    end
  end


  rails_admin do
    label " Serviço"
      label_plural " Serviços"
      navigation_label 'Pagina Serviços'
      navigation_icon 'icon-search'
      weight 7
      edit do
        field :title do
          label "Titulo"
        end 
        field :subtitle do
          label "Subtitulo"
        end
        field :body do
          label "Texto"
        end
        field :images do
          label "imagens"
        end
      end
      list do
        field :title do
          label "Titulo"
        end 
        field :subtitle do
          label "Subtitulo"
        end
        field :body do
          label "Texto"
        end
        field :images do
          label "imagens"
        end
      end

    end
 
end
