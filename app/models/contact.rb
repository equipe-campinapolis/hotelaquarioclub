class Contact < ApplicationRecord
	has_many :contactsections, :dependent => :destroy, :inverse_of => :contact
  	accepts_nested_attributes_for :contactsections, :allow_destroy => true


  	rails_admin do
		label " Como chegar"
    	label_plural " Como chegar"
    	navigation_label 'Pagina Contato'
    	navigation_icon 'icon-inbox'
    	weight 8
    	edit do
    		field :title do
    			label "Titulo"
    		end
    		field :contactsections do
    			label "Descrição"
    		end
    	end
    	list do
    		field :title do
    			label "Titulo"
    		end
    		field :contactsections do
    			label "Descrição"
    		end 
    	end
  	end
end
