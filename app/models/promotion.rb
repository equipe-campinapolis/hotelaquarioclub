class Promotion < ApplicationRecord
	has_one_attached :image
    has_many :promotionsections, :dependent => :destroy, :inverse_of => :promotion
    accepts_nested_attributes_for :promotionsections, :allow_destroy => true

    validates :image, presence: true

	rails_admin do
		label " Promoção"
    	label_plural " Promoções"
    	navigation_label 'Pagina Contato'
    	navigation_icon 'icon-flag'
    	weight 8
    	edit do
    		field :title do
    			label "titulo"
    		end
    		field :big_subtitle do
    			label "Subtitulo maior"
    		end
    		field :subtitle do
    			label "subtitulo menor"
    		end
    		field :visibility do
    			label "visivel ?"
    		end
            field :promotionsections do
                label "item da descrição"
            end
    		field :image do
    			label "imagem"
    		end
    	end
    	list do
    		field :title do
    			label "titulo"
    		end
    		field :big_subtitle do
    			label "Subtitulo maior"
    		end
    		field :subtitle do
    			label "subtitulo menor"
    		end
    		field :visibility do
    			label "visivel ?"
    		end
            field :promotionsections do
                label "item da descrição"
            end
    		field :image do
    			label "imagem"
    		end
    	end
  	end
end
