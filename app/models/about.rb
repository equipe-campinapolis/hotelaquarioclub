class About < ApplicationRecord
	has_one_attached :video #virtual não precisa estar no banco

  validates :video, presence: true
	
  rails_admin do
    label " Sobre nós"
    label_plural " Sobre nós"
    navigation_label 'Pagina Sobre Nós'
    navigation_icon ' icon-pencil'
    weight 7
    edit do
      configure :title do
        label "titulo"
      end
      configure :sub_title do
        label "subtitulo"
      end
      configure :description do
        label "descrição"
      end
      configure :name do
        label "nome do funcionario"
        help "nome e sobrenome"
      end
      configure :kind do
        label "cargo do funcionario"
        help "proprietario/dono/chefe/socio"
      end
    end
    list do
      field :title do
        label "titulo"
      end
      field :sub_title do
        label "subtitulo"
      end
      field :description do
        label "descrição"
      end
      field :name do
        label "funcionario"
      end
      field :kind do
        label "cargo"
      end
    end
  end

end
