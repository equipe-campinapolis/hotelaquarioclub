class Roomsection < ApplicationRecord
  belongs_to :room, :inverse_of => :roomsections


  rails_admin do 
  	label " Item de descrição"
    label_plural " itens de descrição"
    hide
    edit do
    	field :title do
    		label "descrição"
    	end
    end
    list do
    end
  end
end
