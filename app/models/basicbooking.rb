class Basicbooking < ApplicationRecord
    enum enum_status: {"tentando reservar":1,"em processo de reserva":2,"reservado mas não pagou":3,"reservado e pago":4,"reserva cancelada":5}
	
    enum enum_type: {"Opção 1 --- Quarto de Solteiro Simples":1,"Opção 2 --- Quarto de Solteiro Compartilhado":2,"Opção 3 --- Quarto de Casal Simple":3,"Opção 4 --- Quarto de Casal Compartilhado":4}
	
	rails_admin do
		label " Reserva"
    	label_plural " Reservas"
    	navigation_icon 'icon-print'
        show do
            field :arrival do
                label "chegada"
            end
            field :departure do
                label "saida"
            end
            field :name do
                label "nome completo"
            end
            field :enum_type do
                label "tipo de reserva"
            end
            field :enum_status do
                label "status da reserva"
            end
        end
    	edit do
    		field :arrival do
    			label "chegada"
                strftime_format '%d/%m/%Y'
    		end
    		field :departure do
    			label "partida"
                strftime_format '%d/%m/%Y'
    		end
    		field :name do
    			label "nome completo"
    		end
    		field :cel do
    			label "telefone"
    		end
            field :enum_type do
                label "tipo de reserva"
            end
            field :enum_status do
                label "status da reserva"
            end
    	end
    	list do
    		field :created_at do
    			label "feita em"
    		end
    		field :arrival do
    			label "chegada"
    		end
    		field :departure do
    			label "partida"
    		end
    		field :name do
    			label "nome completo"
    		end
    		field :cel do
    			label "telefone"
    		end
    	end
	end

	after_initialize do
    	if new_record?
      		self.enum_status ||= 'tentando reservar'
    	end
  	end

end
