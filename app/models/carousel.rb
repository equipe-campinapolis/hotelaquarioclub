class Carousel < ApplicationRecord
    attr_accessor :remove_primary_image,:remove_images
    has_many_attached :images
    attribute :remove_images, :json
    after_save :delete_images, if: -> { remove_images.present? }

    validates :images, presence: true

    def delete_images
        images.each_with_index { |_, index| images[index].purge if remove_images.include?(index.to_s) }
    end

    after_save do
        primary_image.purge if remove_primary_image == '1'
        Array(remove_images).each do |id|
            images.find_by_id(id).try(:purge)
        end
    end


	rails_admin do
		label " Galeria"
    	label_plural " Galeria"
    	navigation_label 'Pagina Inicio'
    	navigation_icon 'icon-star'
    	weight 1
    	edit do
    		field :title do
    			label "Titulo"
    		end
    		field :images do
    			label "Imagem"
    			help "seleciona uma ou mais imagens"
    		end
    	end
    	list do
    		field :title do
    			label "Titulo"
    		end
    		field :created_at do
    			label "criado em"
    		end
    	end
  	end
end
