class Contactsection < ApplicationRecord
  belongs_to :contact, :inverse_of => :contactsections


  rails_admin do
  	label " Como Chegar"
  	label_plural " Como Chegar"
  	hide
  	edit do
  		field :title do
  			label "titulo"
        end
        field :subtitle do
        	label "descrição"
        end
  	end
  	list do
  		field :title do
 			label "titulo"
        end
        field :subtitle do
        	label "descrição"
        end
 	end
  end
end
