class Team < ApplicationRecord
	has_one_attached :image

    validates :image, presence: true

	rails_admin do
		label " Funcionarios"
    	label_plural " Funcionarios"
    	navigation_label 'Pagina Sobre Nós'
        navigation_icon 'icon-road'
        weight 7
        edit do
    		field :title do
    			label  "Nome do Funcionario"
    		end
    		field :subtitle do
    			label "função do Funcionario"
    		end
    		field :image do
    			label "imagem"
    		end
    	end

    	list do
    		field :title do
    			label "funcionario"
    		end
            field :subtitle do
                label "função"
            end
            field :image do
                label "foto"
            end
    	end
  	end
  	
end
