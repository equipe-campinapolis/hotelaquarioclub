class Promotionsection < ApplicationRecord
  belongs_to :promotion,:inverse_of => :promotionsections
  rails_admin do
  	label "item da promoção"
  	hide
  	edit do
  		field :title do
  			label "item da descrição"
  		end
  	end 
  	list do
  		field :title do
  			label "item da descrição"
  		end
  	end
  end
end
