module ApplicationHelper
	def flash_message(level)
    	case level
        	when "notice" then "alert alert-success"
        	when "success" then "alert alert-success"
        	when "error" then "alert alert-error"
        	when "alert" then "alert alert-danger"
    	end
  	end
end
