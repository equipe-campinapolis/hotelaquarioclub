require_relative 'boot'
require 'rails/all'
Bundler.require(*Rails.groups)
module Motohostelroncador
  class Application < Rails::Application
    config.load_defaults 5.2
    I18n.enforce_available_locales = true
    config.i18n.available_locales = 'pt-BR'
    require Rails.root.join("lib/custom_public_exceptions") 
    config.exceptions_app = CustomPublicExceptions.new(Rails.public_path) 
  end
end
