ActiveSupport::Inflector.inflections do |inflect|
  # Irregulares
  inflect.irregular "país", "países"
  inflect.irregular "boletim", "boletins"
  inflect.irregular "porção", "porções"
  inflect.singular "sanduiches", "sanduiche"
  inflect.singular "sanduíches", "sanduíche"
end