include RailsAdminTagList::SuggestionsHelper
require 'i18n'
I18n.default_locale = :'pt-BR'
RailsAdmin.config do |config|
  config.included_models = ["User","About","Slider",
                            "Carousel","Basicbooking",
                            "Team","Room","Service",
                            "Roomsection","Contact",
                            "Contactsection","Promotion",
                            "Promotionsection"]
  config.parent_controller = '::ApplicationController'
  config.main_app_name = ["MOTO HOSTEL RONCADOR", " - ADMINISTRAÇÃO"]
  config.authenticate_with do
    warden.authenticate! scope: :user 
  end
  config.current_user_method(&:current_user)
  config.authorize_with :pundit
  config.actions do
    dashboard                     
    index                         
    new
    export
    bulk_delete
    show
    edit
    delete
    #show_in_app
  end
end
