Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/site/admin', as: 'rails_admin'
  root to: "inicio#index"
  devise_for :users,path_names: { sign_in: 'entrar', sign_out: 'sair', sign_up: 'registrar' }
  get 'contatos/index'
  get 'quartos/index'
  get 'sobre/index'
  get 'inicio/index'
  get 'servicos/index'
  post '/reserva', to: "application#reserva"
  
  #sempre por ultimo
  #get '*path' => redirect('/') 
  match "/404" => "errors#error404", via: [ :get, :post, :patch, :delete ] 
end
