class AddVisibilityToPromotions < ActiveRecord::Migration[5.2]
  def change
    add_column :promotions, :visibility, :boolean
  end
end
