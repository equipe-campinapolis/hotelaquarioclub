class CreateBasicbookings < ActiveRecord::Migration[5.2]
  def change
    create_table :basicbookings do |t|
      t.date :arrival
      t.date :departure
      t.string :name
      t.string :cel
      t.integer :enum_type
      t.integer :enum_status

      t.timestamps
    end
  end
end
