class CreatePromotionsections < ActiveRecord::Migration[5.2]
  def change
    create_table :promotionsections do |t|
      t.references :promotion, foreign_key: true
      t.string :title

      t.timestamps
    end
  end
end
