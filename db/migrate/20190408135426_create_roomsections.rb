class CreateRoomsections < ActiveRecord::Migration[5.2]
  def change
    create_table :roomsections do |t|
      t.references :room, foreign_key: true
      t.text :title

      t.timestamps
    end
  end
end
