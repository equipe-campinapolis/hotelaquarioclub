class CreateAbouts < ActiveRecord::Migration[5.2]
  def change
    create_table :abouts do |t|
      t.string :title
      t.string :sub_title
      t.text :description
      t.string :name
      t.string :kind

      t.timestamps
    end
  end
end
