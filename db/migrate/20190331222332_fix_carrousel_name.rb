class FixCarrouselName < ActiveRecord::Migration[5.2]
  def change
    rename_table :carrousels, :carousels
  end
end
